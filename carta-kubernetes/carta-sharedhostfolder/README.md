# CARTA deployment on Kubernetes with access to a shared host folder

This configuration and deployment allows you to launch CARTA Kubernetes with the configuration for accessing files from a shared folder between the cluster nodes.

Creates a customised version of the changed `deployment.yml` file:

```
    hostPath: 
        path: <images_folder>
```

where `<images_folder>` is the directory where the images will be accessed for viewing.

You can also add a specific number of replicas by changing the following: 

```
  replicas: 2
```

Then run the next commands: 

```
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```