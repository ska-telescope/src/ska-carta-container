# Simple CARTA deployment on Kubernetes

This configuration and deployment allows you to launch CARTA Kubernetes with the simplest possible configuration.


To test the service run the following:


```
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```