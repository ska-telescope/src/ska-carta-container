# CARTA on Kubernetes

This repository allows you to deploy CARTA (frontend/backend, without `carta-controller`) within Kubernetes for different configurations.


# Building CARTA's image

The first step is to build the CARTA docker image. This image will be used by all deployments in this section of the repository. 

We tag the image with the name `carta:k8s-v1` to distinguish it from the generic CARTA image.

```
cd carta-build
docker build -t carta:k8s-v1 Dockerfile .
```

More documentation and details [here](./carta-build/README.md).

# Simple CARTA deployment on Kubernetes

This configuration and deployment allows you to launch CARTA Kubernetes with the simplest possible configuration, without access to a shared directory (where the images are) and with access through NodePort.

```
cd carta-basic-deployment
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```

More documentation and details [here](./carta-basic-deployment/README.md).

# Simple CARTA deployment on Kubernetes with direct access to an image

This configuration and deployment allows you to launch CARTA Kubernetes with the simplest possible configuration, with access to an image and with external access through NodePort.

```
cd carta-directaccess
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```

More documentation and details [here](./carta-directaccess/README.md).

# CARTA deployment on Kubernetes with access to a shared host folder

This configuration and deployment allows you to launch CARTA Kubernetes  with the configuration for accessing files from a shared folder between the cluster nodes.

```
cd carta-sharedfolder
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```

More documentation and details [here](./carta-sharedhostfolder/README.md).


# CARTA deployment on Kubernetes with PV

This configuration and deployment allows you to launch CARTA Kubernetes with the configuration for the use of PV.

```
cd carta-cephfs
kubectl apply -f carta-pv.yaml
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```

More documentation and details [here](./carta-pv/README.md).

