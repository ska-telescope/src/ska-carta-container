# Build the CARTA image


The first step to using CARTA in Kubernetes is to build the CARTA docker image. This image will be used by all deployments in this section of the repository. 

We label the image ``carta:k8s-v1` to distinguish it from the generic/existing CARTA image.


First clone the repository:

```
git clone https://gitlab.com/ska-telescope/src/ska-carta-container.git
```

Access the folder to build the container:

```
cd ska-carta-container/carta-kubernetes/carta-build/
```

Once here, type the following:

```
docker build -t carta:k8s-v1 Dockerfile .
```

After this you will have the CARTA image inside your docker image registry, ready to be used in Kubernetes.


