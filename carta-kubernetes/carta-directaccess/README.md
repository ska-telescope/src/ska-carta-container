# Simple CARTA deployment on Kubernetes with direct access to an image

This configuration and deployment allows you to launch CARTA Kubernetes in the simplest possible configuration with direct access to an image that can be configured *ad-hoc*. This deployment can be used to show specific images to hundreds/thousands of users for HA.

Creates a customised version of the changed `deployment.yml` file:

```
    value: "<image>"
```

where `<images>` is the file that will be shown when CARTA is opened.

You can also add a specific number of replicas by changing the following: 

```
  replicas: 2
```

```
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```