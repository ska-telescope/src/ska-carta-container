# CARTA deployment on Kubernetes with PV

This configuration and deployment allows you to launch CARTA Kubernetes with the configuration for the use of a Persistent Volume (PV).

Creates a customised version of the changed `pv-chart.yml` file:

```
...
    path: "<images_folder>"
...
```

where `"<images_folder>"` is the directory where the images will be accessed for viewing.

Then you can specify the size of the volume that will be used to store the images:
```
...
    storage: 2Gi
...
```

Then execute the next commands:

```
kubectl apply -f carta-pv.yaml
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```



