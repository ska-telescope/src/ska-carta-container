# SKA CARTA Container

## Overview

This repository provides a set of tools to enable the user to launch a containerised CARTA deployment, in as automated a way as possible.

Whilst this can be performed on any infrastructure running Docker, it is intended as a fast means of deploying CARTA in the cloud.

As a basis, we used the instructions provided here: https://carta-controller.readthedocs.io/en/latest/ubuntu_focal_instructions.html

For users familiar with the Kubernetes (K8s) container orchestration service, a set of K8s manifests and instructions for deploying CARTA with K8s is provided in the `carta-kubernetes` sub-directory.

## Microservices

In order to run CARTA in server mode, three container microservices are required:

1) `carta-core`: An Ubuntu-20.04 based image, which runs the CARTA controller, backend and frontend.
2) `carta-proxy`: An NGINX reverse proxy which manages ingress to the CARTA controller
3) `carta-mongodb`: A Mongo DB instance which the CARTA controller uses to store user preferences

## Setup Instructions

In its simplest form, the full stack of microservices can be started locally via (from the project root directory):
```bash
$ docker-compose -f docker-compose.yml up --build -d
```

Navigating to http://localhost:80 will direct the user to the controller, where a login dialog will be presented.

## Authentication

Initially, we have configured CARTA to use the PAM authentication model, where user credentials in the `carta-core` Ubuntu environment are used to authenticate users. A default user:password of `cartauser:password` is added, though it is recommended this is changed by setting the build arguments `USERNAME` and `PASSWORD` in the `docker-compose.yml` file before deployment.

## Data path configuration

In the `docker-compose.yml` file, an example volume for the `carta-core` container is shown. By mounting a path on the host system in this container, the CARTA service can render data files from the host machine. An additional change to the `carta/etc/config/config.json` file is required to set the default path, e.g. if the data is mounted at `/data/` in the CARTA core container, set:

```
    "rootFolderTemplate": "/data/",
    "baseFolderTemplate": "/data/",
```

## Ingress

By default, traffic to the `carta-proxy` service is routed to the `carta-core` container port that the CARTA Controller is listening on (port 8000 by default). For deploying development instances on local computers, no additional configuration of the proxy may be needed. However, if the CARTA service is to be run on a remote machine accessed over the internet, securing the traffic to the service with SSL encryption is highly recommended.

### Direct access to host server

In the event that this containerised CARTA deployment is running on an externally accessible machine (with the port the `carta-proxy` service listens on open to external traffic) then SSL encryption should be set up for the `carta-proxy` microservice. In brief, this will ideally involve obtaining an SSL certificate and key issued by an appropriate Certificate Authority. These should be copied/mounted into the `carta-proxy` container (example shown in `docker-compose.yml`), and the NGINX config (`proxy/etc/carta.conf`) updated accordingly (example shown in that file).

### Access via an external reverse-proxy

In the event that a different, externally-facing proxy service listens for external requests, then that proxy can handle the SSL termination and forward HTTP traffic to the appropriate `carta-proxy` service port, with no additional changes required.

If the location of the CARTA service is a subdirectory URL, e.g. `https://[domain]/carta/` then some additional config settings will need to be set in `carta/etc/config/config.json`:

```
    "serverAddress": "https://[domain]/carta/",
    "dashboardAddress": "https://[domain]/carta/dashboard/",
    "apiAddress": "https://[domain]/carta/api/",
```

## Issues

### Incorrect URLs to css files (v3.0.1)

An issue with some of the relative resource URLs in CARTA leads to some files (e.g. CSS files) used by the dashboard page not being found; the service appends `/dashboard` even if the `dashboardAddress` is set. A typical error message reads 'Refused to apply style from `[domain]/carta/dashboard/dashboard/templated.css`. The workaround is to add a location to the external NGINX service which redirects traffic from `[domain]/carta/dashboard/dashboard` to `[domain]/carta/dashboard`

# SKA CARTA on Kubernetes

See a development version of CARTA for Kubernetes here: [carta-kubernetes](carta-kubernetes/README.md).